var gulp = require('gulp'),
    less = require('gulp-less'),
    connect = require('gulp-connect'),
    jade = require('gulp-jade'),
    coffee = require('gulp-coffee'),
    css = require('gulp-css'),
    imagemin = require('gulp-imagemin');
var concat = require('gulp-concat');


gulp.task('server', ['less', 'css', 'jade',
                     'html', 'copy', 'copyImages', 'coffeeToJs', 'css', 'combineJs', 'connect', 'watch'])

gulp.task('connect', function() {
  connect.server({
    root: './dist',
    port: 8080,
    livereload: true
  });
});


gulp.task('jade', function () {
  gulp.src('src/**/*.jade')
      .pipe(jade({ pretty: true }))
      .pipe(gulp.dest('.tmp/html'));
});


gulp.task('html', function () {
  gulp.src('.tmp/html/**/*.html')
      .pipe(gulp.dest('dist'))
      .pipe(connect.reload());
});

gulp.task('less', function () {
  gulp.src('src/less/*.less')
      .pipe(less())
      .pipe(gulp.dest('.tmp/css'));
});


gulp.task('css', function () {
  return gulp.src(['src/components/bootstrap/dist/css/bootstrap.css',
                    '.tmp/css/main.css',
                    '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'])
              .pipe(concat('app.css'))
              .pipe(gulp.dest('dist/css'))
              .pipe(connect.reload());
 });


gulp.task('copy', function () {
  gulp.src('src/image/**/*')
      .pipe(imagemin())
      .pipe(gulp.dest('dist/image'));
});

gulp.task('combineJs', function() {
  return gulp.src(['src/components/jquery/dist/jquery.js',
                   'src/components/bootstrap/dist/js/bootstrap.js',
                   '.tmp/js/main.js'])
                .pipe(concat('app.js'))
                .pipe(gulp.dest('dist/js'))
                .pipe(connect.reload());
});


gulp.task('coffeeToJs',function(){
  return gulp.src('src/coffee/*.coffee')
              .pipe(coffee())
             .pipe(gulp.dest('.tmp/js'));

});

gulp.task('copyImages', function() {
  return gulp.src('src/image/**/*.jpg')
             .pipe(gulp.dest('dist/image'))
             .pipe(connect.reload());
});

gulp.task('watch', function() {
  gulp.watch('src/less/*.less', ['less', 'css']);
  gulp.watch('src/**/*.jade', ['jade', 'html']);
  gulp.watch('.tmp/css/**/*.css', ['css']);
  gulp.watch('src/coffee/**/*.coffee', ['coffeeToJs', 'combineJs'])
  gulp.watch('src/image/**/*', ['copy']);
});
