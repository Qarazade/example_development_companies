$(document).ready () ->
  $("a.onur-ajax").click ->
    $.ajax
        url: "https://gist.githubusercontent.com/FaigGara/7b5915e5749a714f4bffde459c963349/raw/d342f1706466356ff6a867b8fa0762a903b4ee15/onur.txt"
        error: (jqXHR, textStatus, errorThrown) ->
          $('body').append "AJAX Error: #{textStatus}"
        success: (data, textStatus, jqXHR) ->
      $.getJSON 'https://gist.githubusercontent.com/FaigGara/7b5915e5749a714f4bffde459c963349/raw/d342f1706466356ff6a867b8fa0762a903b4ee15/onur.txt', (data) ->
          $('#onur').html ('en/developer.html#onur')
          console.log(data.first_name)

$(document).ready () ->
  $("a.duygu-ajax").click ->
    $.ajax
        url: "https://gist.githubusercontent.com/MerveDnmz/6ea48afc884de12fc709bbfa3434df4f/raw/858b52b0ba34d0876cb80e892fe8037e3654ab66/duygu.txt"
        error: (jqXHR, textStatus, errorThrown) ->
          $('body').append "AJAX Error: #{textStatus}"
        success: (data, textStatus, jqXHR) ->
      $.getJSON 'https://gist.githubusercontent.com/MerveDnmz/6ea48afc884de12fc709bbfa3434df4f/raw/858b52b0ba34d0876cb80e892fe8037e3654ab66/duygu.txt', (data) ->
          $('#duygu').html (result)
          console.log(data.first_name) 

$(document).ready () ->
  $("a.faig-ajax").click ->
    $.ajax
        url: "https://gist.githubusercontent.com/MerveDnmz/9910701c26ff88f4176d13b36e2629e8/raw/3485786b80ac1f1330c8ecc7723327f264bb39af/faig.txt"
        error: (jqXHR, textStatus, errorThrown) ->
          $('body').append "AJAX Error: #{textStatus}"
        success: (data, textStatus, jqXHR) ->
      $.getJSON 'https://gist.githubusercontent.com/MerveDnmz/9910701c26ff88f4176d13b36e2629e8/raw/3485786b80ac1f1330c8ecc7723327f264bb39af/faig.txt', (data) ->
          $('#faig').html (result)
          console.log(data.first_name) 

$(document).ready () ->
  $("a.merve-ajax").click ->
    $.ajax
        url: "https://gist.githubusercontent.com/MerveDnmz/28088ca9c29b04b2cf16b3ccef9ee355/raw/d799bfba3f03c184def5f39e5c85a9be1b2241ef/merve.txt"
        error: (jqXHR, textStatus, errorThrown) ->
          $('body').append "AJAX Error: #{textStatus}"
        success: (data, textStatus, jqXHR) ->
      $.getJSON 'https://gist.githubusercontent.com/MerveDnmz/28088ca9c29b04b2cf16b3ccef9ee355/raw/d799bfba3f03c184def5f39e5c85a9be1b2241ef/merve.txt', (data) ->
          $('#merve').html (result)
          console.log(data.first_name) 

$(document).ready () ->
  name = document.location.hash.substr(1)
  
  if name == "duygu"
    url = "https://gist.githubusercontent.com/MerveDnmz/6ea48afc884de12fc709bbfa3434df4f/raw/858b52b0ba34d0876cb80e892fe8037e3654ab66/duygu.txt"
  else if name == "faig"
    url = "https://gist.githubusercontent.com/MerveDnmz/9910701c26ff88f4176d13b36e2629e8/raw/3485786b80ac1f1330c8ecc7723327f264bb39af/faig.txt"
  else if name == "merve"
    url = "https://gist.githubusercontent.com/MerveDnmz/28088ca9c29b04b2cf16b3ccef9ee355/raw/d799bfba3f03c184def5f39e5c85a9be1b2241ef/merve.txt"
  else if name == "onur"
    url = "https://gist.githubusercontent.com/FaigGara/7b5915e5749a714f4bffde459c963349/raw/d342f1706466356ff6a867b8fa0762a903b4ee15/onur.txt"
  
  $.getJSON url, (data) ->
    $('.full_name').text "#{data.first_name} #{data.last_name}"
    $('.bio').text data.bio
    $('.school').text data.education
    $('.experience-1').text data.experience[0].ipos
    $('.experience-2').text data.experience[0].kare_company
    $('.thumbnail').attr "src", "/image/#{name}.jpg"



window.initMap = ->
  uluru = 
    lat: 41.0157166
    lng: 29.0498508
  map = new (google.maps.Map)(document.getElementById('map'),
    zoom: 4
    center: uluru)
  marker = new (google.maps.Marker)(
    position: uluru
    map: map)
  return
